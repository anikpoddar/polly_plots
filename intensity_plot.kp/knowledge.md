---
title: Polly First view plots (intensity and label)
authors:
- Anik_Poddar
tags:
- Plots_for_El_Maven
created_at: 2018-08-07 00:00:00
updated_at: 2018-08-08 16:33:35.532308
tldr: Plotting for El_maven graphs
thumbnail: images/unnamed-chunk-1-1.png
---
```r
      library(ggplot2)
      library(easyGgplot2)
      data_set <- read.csv('dataset.csv')
      #df = apply(data_set, 1, function(r) any(r %in% c('UDP-D-glucose')))
      df = which(data_set$compound=='N-acetyl-glucosamine-1/6-phosphate')
      selected = data_set[df,]
      required_data <- data.frame(selected$isotopeLabel,selected[ , grepl( "X091215" , names( selected ) ) ])
      temp_data <- required_data
      required_data$selected.isotopeLabel <- NULL
    
      
      #ggplot(required_data,aes(required_data[1,],fill = required_data[2,]))+
           #geom_bar( stat="identity", position="fill")
      
      #ggplot2.barplot(data=required_data, xName="Plots",
                #groupName=required_data["selected.isotopeLabel"])
      barplot(as.matrix(required_data), col = rainbow(16))
    ```
    
    ![plot of chunk unnamed-chunk-1](images/unnamed-chunk-1-1.png)


```r
  library(ggplot2)
  z = NCOL(required_data)
  print(z)
```

```
## [1] 16
```

```r
  x <- data.frame()
  #required_data$selected.isotopeLabel <- NULL
  for (i in 1:z){
    required_data[,i] <- data.frame(required_data[,i]/sum(required_data[,i]))
  }   
  
  barplot(as.matrix(required_data), col = rainbow(16))
```

![plot of chunk unnamed-chunk-2](images/unnamed-chunk-2-1.png)

```r
  heatmap(as.matrix(required_data),Colv = NA,Rowv = NA,scale = "row")
```

![plot of chunk unnamed-chunk-2](images/unnamed-chunk-2-2.png)
